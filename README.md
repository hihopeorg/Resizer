# Resizer

**本项目是基于开源项目Resizer进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/hkk595/Resizer ）追踪到原项目版本**

#### 项目介绍

- 项目名称：Resizer
 - 所属系列：ohos的第三方组件适配移植
 - 功能：图片压缩，修改图片尺寸
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/hkk595/Resizer
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：V1.5 , sha1:e502c71b7d5d48987a45178772d51d55604f6e4d

#### 演示效果

<img src="gif/demo.gif"/>

- #### 安装教程

方法1.

1. 编译Resizer的har包: Resizer.har。

2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
   	……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'me.echodev.resizer:resizer:1.0.0'
}
```

 #### 使用说明

1. 使用方式一（实例化）：

 ```java
 
 File resizedImage = new Resizer(this)
         .setTargetLength(1080)
         .setQuality(80)
         .setOutputFormat("JPEG")
         .setOutputFilename("resized_image")
         .setOutputDirPath(storagePath)
         .setSourceImage(originalImage)
         .getResizedFile();
        
 ```


```java
 PixelMap pixelMap = new Resizer(this)
        .setTargetLength(1080)
        .setSourceImage(originalImage)
        .getResizedBitmap();
```

2. 使用方式二（Rxjava）

```java
   final File[] resizedImage = new File[1];
   new Resizer(this)
           .setTargetLength(1080)
           .setQuality(80)
           .setOutputFormat("JPEG")
           .setOutputFilename("resized_image")
           .setOutputDirPath(storagePath)
           .setSourceImage(originalImage)
           .getResizedFileAsFlowable()
           .subscribeOn(Schedulers.io())
           .observeOn(OhosSchedulers.mainThread())
           .subscribe(new Consumer<File>() {
               @Override
               public void accept(File file) {
                   resizedImage[0] = file;
               }
           }, new Consumer<Throwable>() {
               @Override
               public void accept(Throwable throwable) {
                   throwable.printStackTrace();
               }
           });
```

```java
   final Bitmap[] resizedImage = new Bitmap[1];
   new Resizer(this)
           .setTargetLength(1080)
           .setSourceImage(originalImage)
           .getResizedBitmapAsFlowable()
           .subscribeOn(Schedulers.io())
           .observeOn(OhosSchedulers.mainThread())
           .subscribe(new Consumer<Bitmap>() {
               @Override
               public void accept(Bitmap bitmap) {
                   resizedImage[0] = bitmap;
               }
           }, new Consumer<Throwable>() {
               @Override
               public void accept(Throwable throwable) {
                   throwable.printStackTrace();
               }
           });
```

   


### 版本迭代

 - v1.0.0
 
  实现功能:
 1. 实例化方式，图片的压缩输出或PixelMap。
 2. rxjava方式，实现图片压缩为文件或者PixelMap。
 
 #### 版权和许可信息
 ```
   MIT License
    
   Copyright (c) 2017 K.K. Ho
    
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
    
   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.
    
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 ```
