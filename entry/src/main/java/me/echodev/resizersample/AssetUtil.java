/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package me.echodev.resizersample;

import ohos.app.Context;
import ohos.global.resource.Entry;
import ohos.global.resource.Resource;

import java.io.*;

/**
 * Created by linjiang on 05/06/2018.
 */
public class AssetUtil {


    private static String TARGET_BASE_PATH = "";
    public static final String RAWFILE_PATH = "resources/rawfile";

    public static String copyAssertToFiles(Context context) {
        TARGET_BASE_PATH = context.getFilesDir().getPath().concat(File.separator);
        copyFileOrDir(context, RAWFILE_PATH);
        return TARGET_BASE_PATH;
    }

    private static void copyFileOrDir(Context context, String path) {
        ohos.global.resource.ResourceManager resourceManager = context.getResourceManager();
        ohos.global.resource.RawFileEntry rawFileEntry = resourceManager.getRawFileEntry(path);

        String assets[];
        try {
            Entry[] entries = rawFileEntry.getEntries();
            String copyPath = "";
            if(path.contains(RAWFILE_PATH)){
                int index = path.indexOf(RAWFILE_PATH);
                copyPath = path.substring(index+RAWFILE_PATH.length());
            }

            if (entries.length == 0) {
                copyFile(context, path);
            } else {
                String fullPath = TARGET_BASE_PATH + copyPath;
                File dir = new File( fullPath);
                if (!dir.exists() && !path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                    if (!dir.mkdirs())
                        System.err.println("test copyFileOrDir make dir");
                for (int i = 0; i < entries.length; ++i) {
                    String p;
                    if (path.equals(""))
                        p = "";
                    else
                        p = path + "/";

                    if (!path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit")) {
                        copyFileOrDir(context, p + entries[i].getPath());
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void copyFile(Context context, String filename) {
        InputStream in;
        OutputStream out;
        String newFileName = null;

        try {
            ohos.global.resource.ResourceManager resourceManager = context.getResourceManager();
            ohos.global.resource.RawFileEntry rawFileEntry = resourceManager.getRawFileEntry(filename);

            Resource resource = rawFileEntry.openRawFile();

            in = resource;
            String copyPath = "";
            if(filename.contains(RAWFILE_PATH)){
                copyPath = filename.substring(RAWFILE_PATH.length()+1);
            }
            newFileName = TARGET_BASE_PATH + copyPath;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
