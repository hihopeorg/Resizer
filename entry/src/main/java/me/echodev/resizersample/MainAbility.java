/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * The MIT License (MIT)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package me.echodev.resizersample;

import io.reactivex.functions.Consumer;
import io.reactivex.ohos.schedulers.OhosSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.echodev.resizer.Resizer;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.bundle.IBundleManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.IOException;

public class MainAbility extends Ability {
    private final static String[] mPermissions = {"ohos.permission.READ_MEDIA", "ohos.permission.WRITE_MEDIA"};
    private static final int REQUEST_NUMBER = 1;
    Image image, rxjava_Image;
    Text text, rxjavafile_Text;
    public static String value;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        text = (Text) findComponentById(ResourceTable.Id_tip_Text);
        rxjavafile_Text = (Text) findComponentById(ResourceTable.Id_rxjavafile_Text);
        image = (Image) findComponentById(ResourceTable.Id_tip_Image);
        rxjava_Image = (Image) findComponentById(ResourceTable.Id_rxjava_Image);
        value = AssetUtil.copyAssertToFiles(getContext());
        requestPermissions();
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions == null || permissions.length == 0 || grantResults == null || grantResults.length == 0) {
            return;
        }
        Boolean getPermissions = false;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == 0) {
                getPermissions = true;
            } else {
                getPermissions = false;
                return;
            }
        }
        if (getPermissions) {
            try {
                getUITaskDispatcher().delayDispatch(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            settingGetPixelMap();
                        }catch (Exception e){
                            System.out.println("Error :" + e.getMessage());
                        }
                    }
                },3000);
                settingGetFile();
                settingRxjavaPixelMap();
                getUITaskDispatcher().delayDispatch(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            settingRxjavaFile();
                        }catch (Exception e){
                            System.out.println("Error :" + e.getMessage());
                        }
                    }
                },5000);

            } catch (Exception e) {
                System.out.println("Error :" + e.getMessage());
            }
        }
    }


    //请求权限
    public void requestPermissions() {
        if (verifySelfPermission(mPermissions[0]) != IBundleManager.PERMISSION_GRANTED
                || verifySelfPermission(mPermissions[1]) != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission(mPermissions[0]) || canRequestPermission(mPermissions[0])) {
                requestPermissionsFromUser(
                        mPermissions, REQUEST_NUMBER);
            }
        } else {
            try {
                getUITaskDispatcher().delayDispatch(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            settingGetPixelMap();
                        }catch (Exception e){
                            System.out.println("Error :" + e.getMessage());
                        }
                    }
                },3000);
                settingGetFile();
                settingRxjavaPixelMap();
                getUITaskDispatcher().delayDispatch(new Runnable() {
                    @Override
                    public void run() {
                        settingRxjavaFile();
                    }
                },5000);

            } catch (Exception e) {
                System.out.println("Error :" + e.getMessage());
            }
        }
    }

    //示例：获取PixelMap 实例
    public void settingGetPixelMap() throws Exception {
        File GetPixelMapFile = new File(value + "default.jpg");
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        sourceOptions.formatHint = "image/jpg";
        PixelMap pixelMap = new Resizer(this)
                .setTargetLength(1080)
                .setQuality(80)
                .setSourceImage(GetPixelMapFile)
                .getResizedBitmap();
        image.setPixelMap(pixelMap);
    }

    //示例：获取File 实例
    public void settingGetFile() {
        File GetFile = new File(value + "default.jpg");
        File settingGetFile = null;
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        sourceOptions.formatHint = "image/jpg";
        try {
            settingGetFile = new Resizer(this)
                    .setTargetLength(1080)
                    .setQuality(80)
                    .setOutputFormat("jpg")
                    .setOutputFilename("cccccc")
                    .setSourceImage(GetFile)
                    .setOutputFormat(sourceOptions)
                    .getResizedFile();
        } catch (IOException e) {
            System.out.println("出错了 " + e.getMessage());
        }
        text.setText(settingGetFile != null ? "图像文件保存成功" : "图像文件保存失败");
    }

    //示例：使用个Rxjava2 方式获取PixelMap 实例
    private void settingRxjavaPixelMap() throws Exception {
        final PixelMap[] resizedImage = new PixelMap[1];
        File RxjavaPixelMapFile = new File(value + "default_2.png");

        new Resizer(this)
                .setTargetLength(1080)
                .setQuality(80)
                .setSourceImage(RxjavaPixelMapFile)
                .getResizedBitmapAsFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(OhosSchedulers.mainThread())
                .subscribe(new Consumer<PixelMap>() {
                    @Override
                    public void accept(PixelMap pixelMap) throws Exception {
                        resizedImage[0] = pixelMap;
                        rxjava_Image.setPixelMap(pixelMap);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                });
    }

    //示例：使用个Rxjava2 方式获取File 实例
    private void settingRxjavaFile() {
        File RxjavaFile = new File(value + "default_2.png");

        new Resizer(this)
                .setTargetLength(1080)
                .setQuality(80)
                .setOutputFormat("PNG")
                .setOutputFilename("resized_image")
                .setSourceImage(RxjavaFile)
                .getResizedFileAsFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(OhosSchedulers.mainThread())
                .subscribe(new Consumer<File>() {
                    @Override
                    public void accept(File file) throws Exception {
                        System.err.println("保存图片");
                        rxjavafile_Text.setText("Rxjava图像文件保存成功");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        rxjavafile_Text.setText("Rxjava图像文件保存成功失败 ：" + throwable.getMessage());
                    }
                });
    }
}
