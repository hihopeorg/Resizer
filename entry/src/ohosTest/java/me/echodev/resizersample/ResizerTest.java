/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.echodev.resizersample;

import io.reactivex.Flowable;
import io.reactivex.ohos.schedulers.OhosSchedulers;
import io.reactivex.schedulers.Schedulers;
import junit.framework.TestCase;
import me.echodev.resizer.Resizer;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import java.io.File;
import java.io.IOException;

import static me.echodev.resizersample.EventHelper.getContext;


public class ResizerTest extends TestCase {
    Resizer resizer = null;
    MainAbility mAbility;
    String value = null;

    @Before
    public void setUp() throws Exception {
        mAbility = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(2000);
        value = AssetUtil.copyAssertToFiles(getContext());
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }

    public void testGetResizedFile() {
        File GetFile = new File(value + "default.jpg");
        File settingGetFile = null;
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        sourceOptions.formatHint = "image/jpg";
        try {
            settingGetFile = new Resizer(mAbility.getContext())
                    .setTargetLength(1080)
                    .setQuality(80)
                    .setOutputFormat("jpg")
                    .setOutputFilename("Test_01")
                    .setSourceImage(GetFile)
                    .setOutputFormat(sourceOptions)
                    .getResizedFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(settingGetFile);
    }

    public void testGetResizedBitmap() {
        PixelMap pixelMap = null;
        try {
            pixelMap = settingGetPixelMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(pixelMap);
    }

    public void testGetResizedFileAsFlowable() {
        File RxjavaFile = new File(value + "default_2.png");
        Flowable<File> fileFlowable = new Resizer(getContext())
                .setTargetLength(1080)
                .setQuality(80)
                .setOutputFormat("PNG")
                .setOutputFilename("resized_image")
                .setSourceImage(RxjavaFile)
                .getResizedFileAsFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(OhosSchedulers.mainThread());
        Assert.assertNotNull(fileFlowable);
    }

    public void testGetResizedBitmapAsFlowable() {
        File RxjavaPixelMapFile = new File(value + "default_2.png");
        Flowable<PixelMap> pixelMapFlowable = new Resizer(getContext())
                .setTargetLength(1080)
                .setQuality(80)
                .setSourceImage(RxjavaPixelMapFile)
                .getResizedBitmapAsFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(OhosSchedulers.mainThread());
        Assert.assertNotNull(pixelMapFlowable);
    }

    public PixelMap settingGetPixelMap() throws Exception {
        File GetPixelMapFile = new File(value + "default.jpg");
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        sourceOptions.formatHint = "image/jpg";
        return new Resizer(getContext())
                .setTargetLength(1080)
                .setQuality(10)
                .setSourceImage(GetPixelMapFile)
                .getResizedBitmap();

    }
}