/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.echodev.resizersample;

import junit.framework.TestCase;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static me.echodev.resizersample.EventHelper.getContext;

public class MainAbilityTest extends TestCase {
    MainAbility mAbility;

    @Before
    public void setUp() throws Exception {
        mAbility = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(2000);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        mAbility.stopAbility(mAbility.getIntent());
        Thread.sleep(2000);
    }

    @Test
    public void testSettingGetPixelMap() throws Exception {
        Thread.sleep(3000);
        Image image = (Image) mAbility.findComponentById(ResourceTable.Id_tip_Image);
        PixelMap pixelMap = image.getPixelMap();
        Assert.assertTrue(pixelMap != null);
    }

    @Test
    public void testSettingGetFile() throws Exception {
        Thread.sleep(3000);
        Text text = (Text) mAbility.findComponentById(ResourceTable.Id_tip_Text);
        String value = text.getText();
        Assert.assertTrue(value.equals("图像文件保存成功"));
    }

    @Test
    public void testSettingRxjavaPixelMap() throws Exception {
        Thread.sleep(3000);
        mAbility = EventHelper.startAbility(MainAbility.class);
        Image image = (Image) mAbility.findComponentById(ResourceTable.Id_rxjava_Image);
        PixelMap pixelMap = image.getPixelMap();
        Assert.assertTrue(pixelMap != null);
    }

    @Test
    public void testAjavaFile() throws Exception {
        Thread.sleep(3000);
        String value = ((Text) mAbility.findComponentById(ResourceTable.Id_rxjavafile_Text)).getText();
        System.err.println("save data " + getContext().getFilesDir().getPath()+"/Pictures"+"/resized_image.png");

        Assert.assertTrue(value.equals("Rxjava图像文件保存成功"));
    }


}