package me.echodev.resizer.util;

import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by K.K. Ho on 3/9/2017.
 */

public class FileUtils {

    public static String getOutputFilePath(ImageSource.SourceOptions sourceOptions, String outputDirPath, String outputFilename, File sourceImage) {
        String originalFileName = sourceImage.getName();
        String targetFileName;
        String targetFileExtension = ".jpg";
        if (sourceOptions.formatHint.contains("jpeg")) {
            targetFileExtension = ".jpg";
        } else if (sourceOptions.formatHint.contains("png")) {
            targetFileExtension = ".png";
        } else if (sourceOptions.formatHint.contains("webp")) {
            targetFileExtension = ".webp";
        }

        if (outputFilename == null) {
            int extensionIndex = originalFileName.lastIndexOf('.');
            if (extensionIndex == -1) {
                targetFileName = originalFileName + targetFileExtension;
            } else {
                targetFileName = originalFileName.substring(0, extensionIndex) + targetFileExtension;
            }
        } else {
            targetFileName = outputFilename + targetFileExtension;
        }
        return outputDirPath + File.separator + targetFileName;
    }

    public static void writeBitmapToFile(PixelMap bitmap, int quality, String filePath) throws IOException {
        FileOutputStream fileOutputStream = null;
        ImagePacker imagePacker = null;

        try {
            imagePacker = ImagePacker.create();
            fileOutputStream = new FileOutputStream(filePath);
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.quality = quality;
            imagePacker.initializePacking(fileOutputStream, null);
            imagePacker.addImage(bitmap);
            imagePacker.finalizePacking();
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }
    }
}
