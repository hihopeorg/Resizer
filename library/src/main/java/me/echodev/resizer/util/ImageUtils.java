package me.echodev.resizer.util;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.IOException;

public class ImageUtils {

    public static File getScaledImage(int targetLength, int quality, ImageSource.SourceOptions sourceOptions,
                                      String outputDirPath, String outputFilename, File sourceImage) throws IOException {
        File directory = new File(outputDirPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        // Prepare the new file name and path
        String outputFilePath = FileUtils.getOutputFilePath(sourceOptions, outputDirPath, outputFilename, sourceImage);

        // Write the resized image to the new file
        PixelMap scaledBitmap = getScaledBitmap(targetLength, sourceImage, sourceOptions);

        FileUtils.writeBitmapToFile(scaledBitmap, quality, outputFilePath);
        File file = new File(outputFilePath);
        return file;
    }

    public static PixelMap getScaledBitmap(int targetLength, File sourceImage, ImageSource.SourceOptions packingOptions) {
        ImageSource imageSource = null;
        imageSource = ImageSource.create(sourceImage, packingOptions);
        PixelMap pixelmap = imageSource.createPixelmap(null);

        // Get the dimensions of the original bitmap
        int originalWidth = pixelmap.getImageInfo().size.width;
        int originalHeight = pixelmap.getImageInfo().size.height;

        float aspectRatio = (float) originalWidth / originalHeight;

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size();

        // Calculate the target dimensions
        int targetWidth, targetHeight;

        if (originalWidth > originalHeight) {
            targetWidth = targetLength;
            targetHeight = Math.round(targetWidth / aspectRatio);
        } else {
            aspectRatio = 1 / aspectRatio;
            targetHeight = targetLength;
            targetWidth = Math.round(targetHeight / aspectRatio);
        }
        initializationOptions.size.width = targetWidth;
        initializationOptions.size.height = targetHeight;

        return PixelMap.create(pixelmap, initializationOptions);
    }
}
